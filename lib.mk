# Tryin' real hard not to depend on https://gmsl.sourceforge.io/
# See the README.md
# TODO: Check https://www.gnu.org/software/make/manual/html_node/Special-Variables.html#Special-Variables
# .RECIPEPREFIX := | w00t!
#
##-------- INIT VARS --------##
# Don't echo commands unless $DEBUG
$(DEBUG).SILENT:

# Execute everything in a single shell
.ONESHELL:

# Delete the default suffixes & rules (make -d)
.SUFFIXES:
%: %,v
%: RCS/%,v
%: RCS/%
%: s.%
%: SCCS/s.%
