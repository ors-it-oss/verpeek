use clap::{crate_version, Clap};
use isahc::prelude::*;
use serde::Serialize;

/// Easily peek at release versions of assorted packages
#[derive(Clap)]
#[clap(version = crate_version ! ())]
struct Opts {
    #[clap(short, long, env = "VERPEEK_CONFIG", default_value = "verpeek.toml")]
    config: String,
    // /// Some input. Because this isn't an Option<T> it's required to be used
    // input: String,
    /// Verbosity, up to three times
    #[clap(short, long, parse(from_occurrences))]
    verbose: i8,
    // #[clap(subcommand)]
    // subcmd: SubCommand,
}

// #[derive(Clap)]
// enum SubCommand {
//     Get(CmdGet),
// }
//
// /// A subcommand for controlling testing
// #[derive(Clap)]
// struct CmdGet {
//     /// Print debug info
//     #[clap(short)]
//     debug: bool
// }

fn main() -> Result<(), isahc::Error> {
    let opts: Opts = Opts::parse();

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    println!("Value for config: {}", opts.config);
    // println!("Using input file: {}", opts.input);

    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
    match opts.verbose {
        0 => println!("No verbose info"),
        1 => println!("Some verbose info"),
        2 => println!("Tons of verbose info"),
        3 | _ => println!("Don't be crazy"),
    }

    //
    // // You can handle information about subcommands by requesting their matches by name
    // // (as below), requesting just the name used, or both at the same time
    // match opts.subcmd {
    //     SubCommand::Get(t) => {
    //         if t.debug {
    //             println!("Printing debug info...");
    //         } else {
    //             println!("Printing normally...");sudo
    //         }
    //     }
    // }

    let deserialized: Point =
    //
    // serde_json::from_str(&serialized).unwrap();


    // Send a GET request and wait for the response headers.
    // Must be `mut` so we can read the response body.
    let mut response = isahc::get("http://example.org")?;

    // Print some basic info about the response to standard output.
    println!("Status: {}", response.status());
    println!("Headers: {:#?}", response.headers());

    // Read the response body as text into a string and print it.
    print!("{}", response.text()?);

    Ok(())
}
