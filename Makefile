include lib.mk

.PHONY: cargo1 cargo2

cargo1:
	set -x
	podman --log-level info run --rm -it \
	  --read-only \
	  --userns=keep-id  \
	  --env USER=$$USER \
	  --volume $(PWD):/src/:rw \
	registry.gitlab.com/ors-it-oss/oci:rust

cargo2:
	set -x
	podman --log-level info run --rm -it \
	  --volume $(PWD):/src/:rw \
	registry.gitlab.com/ors-it-oss/oci:rust
